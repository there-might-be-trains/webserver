package net.tmbt

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default

/**
 * Command line option parser. An instance of this is created in the main method and stored in Application.kt at
 * toplevel, so other parts of the server can access it
 */
class Options(parser: ArgParser) {
    /**
     * Path to the directory on disk that contains static content to serve
     */
    val staticContent by parser
        .storing("-f", "--static", help = "static web content directory. defaults to 'web'", argName = "FILE")
        .default("web")

    val port by parser
        .storing("-p", "--port", help = "TCP port. defaults to 8080") { toInt() }.default(8080)

    val host by parser
        .storing("-i", "--ip", help = "listen to this host. defaults to 0.0.0.0").default("0.0.0.0")
}