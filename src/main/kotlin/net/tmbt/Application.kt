package net.tmbt

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.ShowHelpException
import io.ktor.server.application.Application
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import net.tmbt.plugins.configureRouting

lateinit var cliOptions: Options
    private set

fun main(args: Array<String>) {
    try {
        cliOptions = ArgParser(args).parseInto(::Options)
    } catch (e: ShowHelpException) {
        e.printAndExit()
    }

    embeddedServer(Netty, port = cliOptions.port, host = cliOptions.host, module = Application::module)
        .start(wait = true)
}

fun Application.module() {
    configureRouting()
}
