# TMBT Webserver

A HTTP webserver that serves both the frontend via static routes
and provides access to all APIs required by the frontend code.
The server is written in Kotlin using [Ktor](https://ktor.io);
the frontend code is located in another [repository](https://gitlab.com/there-might-be-trains/tmbt-website).

## Usage

The server accepts some command line options. Use `--help` to get short names.

* `--static` path to a directory from which static content is served
* `--port` server will bind this port
* `--ip` server will listen to this IP
* `--help` displays a help page

## Development

The repository for frontend code is embedded as a git submodule.
When checking out the repository, use `git submodule update --init --recursive` to clone the frontend code into your
local file tree.
To update the state of the frontend repository, use `git submodule foreach git pull`.
The submodule is located in `run/web`.
By default the webserver will serve static content from a relative directory called `web`,
however you don't want to serve the entire frontend repository, so to test the server during development,
use the option `--static web/src` (if you are running the webserver from within `run`). 
